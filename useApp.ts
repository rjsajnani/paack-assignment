import {useState} from 'react';
import {Platform} from 'react-native';
import {
  request,
  PERMISSIONS,
  RESULTS,
  requestMultiple,
  checkMultiple,
} from 'react-native-permissions';
const OS = Platform.OS;
export const useApp = () => {
  const [permission, setPermission] = useState(false);
  const requestPermission = () => {
    if (OS === 'ios') {
      request(PERMISSIONS.IOS.LOCATION_ALWAYS).then((result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            setPermission(false);
            break;
          case RESULTS.DENIED:
            setPermission(false);
            break;
          case RESULTS.LIMITED:
            setPermission(true);
            break;
          case RESULTS.GRANTED:
            setPermission(false);
            break;
          case RESULTS.BLOCKED:
            setPermission(false);
            break;
          default:
            setPermission(false);
            break;
        }
      });
    } else {
      requestMultiple([
        PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
        PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
      ]).then((statuses) => {
        statuses[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION] === 'granted'
          ? setPermission(true)
          : setPermission(false);
      });
    }
  };

  const checkPermission = () => {
    if (OS === 'ios') {
      checkMultiple([PERMISSIONS.IOS.LOCATION_ALWAYS]).then((statuses) => {
        statuses[PERMISSIONS.IOS.LOCATION_ALWAYS] === 'granted'
          ? setPermission(true)
          : setPermission(false);
      });
    } else {
      checkMultiple([
        PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION,
        PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION,
      ]).then((statuses) => {
        statuses[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION] === 'granted'
          ? setPermission(true)
          : setPermission(false);
      });
    }
  };

  return {
    requestPermission,
    checkPermission,
    permission,
  };
};
