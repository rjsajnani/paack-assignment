# Paack Assignment

This is an application that shows you list of deliveries and it's details.
Once a delivery is active we need to capture some information and pass it to the API.

[API](http://paack-api.herokuapp.com).

The project uses:

- [React Native](https://facebook.github.io/react-native/docs/getting-started) for iOS & Android app development
- [TypeScript](https://www.typescriptlang.org/docs/home.html) for type-safety
- [Redux](https://redux.js.org/api/api-reference) for state management
- [Background Actions](https://github.com/Rapsssito/react-native-background-actions) [Background timer](https://github.com/ocetnik/react-native-background-timer) for managing background task for Android and iOS
- [React Native Paper](https://callstack.github.io/react-native-paper/) used the UI components and styling provided
- [React native device info](https://github.com/react-native-device-info/react-native-device-info#installation) mainly used to get the device battery info
- [React native geolocation-service](https://github.com/Agontuk/react-native-geolocation-service) to get device's current geo-location


## API

The `API` was developed using [JSON-SERVER](https://github.com/typicode/json-server) and hosted using [Heroku](http://heroku.com).

## Supported features

| Deliveries                         |           Delivery Details           |             Active Delivery |
| ---------------------------------- | :----------------------------------: | -----------------------------: |
| ![deliveries](demo/deliveries.png) | ![details](demo/details.png) | ![active_delivery](demo/active_delivery.png) |


## Structure of the project

The folder structure of the project looks as follows:

```
── App.tsx
src
└── components
└── helpers
└── mocks
└── navigation
└── screen
│ └── Deliveries
└── services
├── store
│ └── slice
└── types
```
### components

Contains reusable UI components.

### helper

Used mainly to collect tracking info

### mocks

Contains the default values used for mocking the test cases

### navigation

Contains navigation related files (`Stack Navigators`, `NavBar`, etc.).

### screen

Contains screens of the app. Each screen has dedicated folders for components and hooks.

### services

Contains the `API` calls required by the app

### store

- selectors: `Selectors` to retrieve and compute derived data from the `Redux state`
- slices: `Slices` of the `Redux state` which contain `actions` and `reducers`

### types

Contains the types used throughout the project.

---
## Running

Install dependencies:

```
yarn

npx pod-install ios
```

Start packager:

```
yarn start
```

### iOS:

Run the app:

```
npx react-native run-ios
```

If you prefer Xcode rather than command line:

- open `./ios/PaackAssignment.xcworkspace` in Xcode
- select a simulator or a device
- hit the Run button

### Android:

Have an Android emulator running (quickest way to get started), or a device connected
###### **make sure you have added local.properties
Run the app:

```
npx react-native run-android
```

### Limitations

#### iOS
Background jobs/proccess for iOS devices genrerally only run for 30 seconds once the application is in background, they automatically are killed by the OS after 30 seconds.

Once the iOS application has been closed then the background job also is stopped.


### API

Unable to append to an array inside an object a limiation from json-server itself, thus the application replaces the tracking info every time it calls the `API`