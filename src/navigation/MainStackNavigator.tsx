//Used to create a stack navigation
// -Deliveries - to show the list of the deliveries
// - Details - to show the details of a particular delivery

import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Deliveries} from '../screen/Deliveries/Deliveries';
import DeliveryDetails from '../screen/Deliveries/DeliveryDetails';

export type RootStackParamList = {
  Deliveries: undefined;
  DeliveryDetails: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

export const MainStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen name="Deliveries" component={Deliveries} />
      <Stack.Screen
        name="DeliveryDetails"
        component={DeliveryDetails}
        options={{
          headerTitle: 'Details',
        }}
      />
    </Stack.Navigator>
  );
};
