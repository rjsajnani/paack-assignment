export type DeliveriesObject = {
  id: number,
  address: string,
  latitude: number,
  longitude: number,
  customer_name: string,
};

//delivery details
export type DeliveryDetails =
  | {
      requires_signature?: boolean,
      special_instructions?: string,
    }
  | DeliveriesObject;
