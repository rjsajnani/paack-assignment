// This helper file is used to collect geo-location of the device, battery level and delivery id
// It then creates an object and stores it in tracking info, every 10 seconds an API call is made
// to pass the info

// iOS devices -  Background timmer package is used, it has a limitation once the device goes into background,
// the job is automatically cancelled by the OS after 30 seconds

// Android devices - Backgrounf Actions package is used, once the proccess has started it will only be stopped,
// once we call the stop function and also it displays a notification icon

import BackgroundJob from 'react-native-background-actions';
import BackgroundTimer from 'react-native-background-timer';
import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Geolocation, {
  GeoError,
  GeoPosition,
} from 'react-native-geolocation-service';
import {postTrackingInfo} from '../services';

export type TrackingInfo = {
  driverId: number;
  trackingData: TrackingData[];
};

type TrackingData = {
  latitude: number;
  longitude: number;
  delivery_id: number;
  battery_level: number;
  timestamp: number;
};

type CreateTrackingData = {
  coords: GeoPosition;
  deliveryId: number;
  batteryLevel: number;
};

let trackingInfo: TrackingInfo = {driverId: 100, trackingData: []};

const sleep = (time: number) =>
  new Promise((resolve) => setTimeout(() => resolve({}), time));

const backgroundJob = async (taskData: any) => {
  const {delay, deliveryId} = taskData;
  if (Platform.OS === 'ios') {
    BackgroundTimer.runBackgroundTimer(() => {
      startTrackingData(deliveryId);
    }, delay);
  }
  await new Promise(async () => {
    // For loop with a delay
    for (let i = 0; BackgroundJob.isRunning(); i++) {
      startTrackingData(deliveryId);
      await sleep(delay);
    }
  });
};

const createTrackingData = ({
  coords,
  deliveryId,
  batteryLevel,
}: CreateTrackingData) => {
  trackingInfo.trackingData.push({
    latitude: coords.coords.latitude,
    longitude: coords.coords.longitude,
    delivery_id: deliveryId,
    battery_level: batteryLevel,
    timestamp: coords.timestamp,
  });
  if (trackingInfo.trackingData.length >= 10) {
    postTrackingInfo(trackingInfo);
    trackingInfo.trackingData = [];
  }
};

const getBatteryInfo = (): Promise<number> => {
  return DeviceInfo.getBatteryLevel().then((batteryLevel) => {
    return batteryLevel;
  });
};

const startTrackingData = (deliveryId: number) => {
  Geolocation.getCurrentPosition(
    async (position: GeoPosition) => {
      let currentBatteryLevel = await getBatteryInfo();
      createTrackingData({
        coords: position,
        deliveryId: deliveryId,
        batteryLevel: currentBatteryLevel * 100,
      });
    },
    (error: GeoError) => {
      // See error code charts below.
      console.log(error.code, error.message);
    },
    {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
  );
};

export const triggerBackgrounfJob = async (id: number, restart: boolean) => {
  const options = {
    taskName: 'Paack Delivery',
    taskTitle: ` Delivery #${id} being tracked`,
    taskDesc: '',
    taskIcon: {
      name: 'ic_launcher',
      type: 'mipmap',
    },
    color: '#00a3e0',
    linkingURI: `paackAssignment://details/${id}`,
    parameters: {
      delay: 1500,
      deliveryId: id,
    },
  };

  const isJobRunning = BackgroundJob.isRunning();
  if (!isJobRunning) {
    await BackgroundJob.start(backgroundJob, options);
  } else {
    postTrackingInfo(trackingInfo);
    trackingInfo.trackingData = [];
    await BackgroundJob.stop();
    BackgroundTimer.stopBackgroundTimer();
    if (restart) {
      await BackgroundJob.start(backgroundJob, options);
    }
  }
};
