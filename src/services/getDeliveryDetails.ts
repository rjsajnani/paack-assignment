// This is the API service to get delivery details by id and dispatch an action once it receives them.

import axios from 'axios';
import {Dispatch} from 'react';
import {API_BASE_URL} from '../contants';
import {deliveryDetailsAction} from '../store/slice/deliveryDetailsSlice';

const {
  getDetailsRequest,
  getDetailsSuccess,
  getDetailsFailed,
} = deliveryDetailsAction;

export const fetchDetails = (id: number) => async (
  dispatch: Dispatch<{}>,
): Promise<void> => {
  const url = API_BASE_URL + `/delivery/${id}`;
  try {
    dispatch(getDetailsRequest());
    axios
      .get(url)
      .then(function (response) {
        // handle success
        if (Object.keys(response).length > 0) {
          dispatch(getDetailsSuccess({details: response.data}));
        } else {
          dispatch(getDetailsFailed({errorMessage: 'No data'}));
        }
      })
      .catch(function (error) {
        // handle error
        dispatch(getDetailsFailed({errorMessage: error.message}));
      });
  } catch (error) {
    dispatch(getDetailsFailed({errorMessage: error.message}));
  }
};
