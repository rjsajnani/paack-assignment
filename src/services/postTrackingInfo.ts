// This is the API service to post the tracking info every 10 seconds

// Currently i'm replacing the data in the API every 10 seconds, as this is the limitations from
// json-server (unable to just update the array within)

// ideally I will be passing the info and the API will be able to append it the the array. This will
// help maintain a record

import axios from 'axios';
import {API_BASE_URL} from '../contants';
import {TrackingInfo} from '../helper/trackingData';

export const postTrackingInfo = (trackingInfo: TrackingInfo) => {
  const url = API_BASE_URL + '/tracked_info';
  if (Object.keys(trackingInfo).length >= 1) {
    axios
      .post(url, trackingInfo)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
};
