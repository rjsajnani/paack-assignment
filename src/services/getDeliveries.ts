// This is the API service to get all the deliveries and dispatch an action once it receives them

import axios from 'axios';
import {Dispatch} from 'react';
import {API_BASE_URL} from '../contants';
import {deliveriesAction} from '../store/slice/deliveriesSlice';

const {
  getDeliveriesRequest,
  getDeliveriesSuccess,
  getDeliveriesFailed,
} = deliveriesAction;

export const fetchDeliveries = () => async (
  dispatch: Dispatch<{}>,
): Promise<void> => {
  const url = API_BASE_URL + '/deliveries';
  try {
    dispatch(getDeliveriesRequest());
    axios
      .get(url)
      .then(function (response) {
        // handle success
        if (Object.keys(response).length > 0) {
          dispatch(getDeliveriesSuccess({deliveries: response.data}));
        } else {
          dispatch(getDeliveriesFailed({errorMessage: 'No data'}));
        }
      })
      .catch(function (error) {
        // handle error
        dispatch(getDeliveriesFailed({errorMessage: error.message}));
      });
  } catch (error) {
    dispatch(getDeliveriesFailed({errorMessage: error.message}));
  }
};
