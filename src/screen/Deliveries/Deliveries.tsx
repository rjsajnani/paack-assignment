//To display a list of all available deliveries

import React, {FC} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';

import {DeliveriesObject} from '../../types/deliveries';
import {useDeliveries} from './hooks/useDeliveries';
import {ErrorText, LoadingScreen} from '../../components';
import DeliveriesListItem from './components/DeliveriesListItem';

export const Deliveries: FC<{}> = () => {
  const {
    deliveries,
    status,
    activeDeliveryId,
    redirectToDetails,
  } = useDeliveries();

  const renderItem = ({item}: {item: DeliveriesObject}) => {
    return (
      <DeliveriesListItem
        delivery={item}
        activeDeliveryId={activeDeliveryId}
        onPress={(id: number) => redirectToDetails(id)}
      />
    );
  };
  const keyExtractor = (item: DeliveriesObject) => item.id.toString();

  const renderList = () => {
    if (status !== 'error') {
      return (
        <FlatList
          data={deliveries}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
        />
      );
    } else {
      return <ErrorText message={'No records found'} />;
    }
  };
  return status === 'loading' ? (
    <LoadingScreen />
  ) : (
    <View style={styles.container}>{renderList()}</View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
});
