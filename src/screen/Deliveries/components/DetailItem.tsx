// Mainly used to create the details UI view

import React, {FC} from 'react';
import {View, StyleSheet} from 'react-native';
import {Subheading} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';

interface Props {
  label: string;
  value: any;
}

// create a component
const DetailItem: FC<Props> = ({label, value}) => {
  return (
    <View style={styles.container}>
      <Subheading style={styles.label}>{label}:</Subheading>
      {typeof value === 'boolean' ? (
        <Icon
          name={value ? 'check' : 'times'}
          color={value ? 'green' : 'red'}
          size={20}
          style={styles.icon}
        />
      ) : (
        <Subheading style={styles.value}>{value}</Subheading>
      )}
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginVertical: 5,
  },
  label: {
    marginHorizontal: 5,
    width: '40%',
  },
  value: {
    width: '60%',
    flexWrap: 'wrap',
    fontSize: 16,
  },
  icon: {
    alignSelf: 'center',
  },
});

//make this component available to the app
export default DetailItem;
