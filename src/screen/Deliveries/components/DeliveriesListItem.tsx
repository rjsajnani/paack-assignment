// Used to create the UI for delivery list

// Using the List compoment provider by React native paper

import React, {FC} from 'react';
import {DeliveriesObject} from '../../../types/deliveries';
import {Colors, Divider, List} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

interface Props {
  delivery: DeliveriesObject;
  onPress(id: number): void;
  activeDeliveryId: number | '';
}

// create a component
const DeliveriesListItem: FC<Props> = ({
  delivery,
  onPress,
  activeDeliveryId,
}) => {
  return (
    <>
      <List.Item
        style={delivery.id === activeDeliveryId && styles.activeListItem}
        title={delivery.customer_name}
        description={delivery.address}
        descriptionStyle={styles.description}
        right={() => (
          <Icon style={styles.icon} name="chevron-right" size={16} />
        )}
        onPress={() => onPress(delivery.id)}
      />
      <Divider />
    </>
  );
};

const styles = StyleSheet.create({
  description: {
    width: '75%',
    marginVertical: 5,
  },
  icon: {
    alignSelf: 'center',
  },
  activeListItem: {
    backgroundColor: Colors.green400,
  },
});

export default DeliveriesListItem;
