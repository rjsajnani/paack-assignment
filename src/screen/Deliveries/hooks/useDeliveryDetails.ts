// Used to get the redux state and define some fucntions that will be used by the details page

// Geolocation is being called here once to see if we can get the location info from the device.

// AsynsStorage call over here is used to store the current active delivery id upon start
//  and upon end it will replace it with an empty string

import Geolocation, {GeoError} from 'react-native-geolocation-service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useApp} from '../../../../useApp';
import {triggerBackgrounfJob} from '../../../helper/trackingData';
import {RootState} from '../../../store';
import {activeDeliveryAction} from '../../../store/slice/activeDelivery';

export const useDeliveryDetails = () => {
  const dispatch = useDispatch();
  const {checkPermission, permission} = useApp();

  useEffect(() => {
    checkPermission();
  });

  const {
    details,
    status,
    errorMessage,
    activeDeliveryId,
    deliveryId,
  } = useSelector((state: RootState) => ({
    details: state.deliveryDetails.details,
    status: state.deliveryDetails.status,
    errorMessage: state.deliveryDetails.errorMessage,
    deliveryId: state.deliveryDetails.details.id,
    activeDeliveryId: state.activeDelivery.deliveryId,
  }));

  const [snackBarVisible, setSnackBarVisible] = useState(false);

  const previousActiveDeliveryId = activeDeliveryId;

  const onToggleSnackBar = () => setSnackBarVisible(!snackBarVisible);

  const onDismissSnackBar = () => setSnackBarVisible(false);

  const startBackgroundJob = async () => {
    if (permission) {
      Geolocation.getCurrentPosition(
        async () => {
          if (activeDeliveryId === deliveryId) {
            dispatch(activeDeliveryAction.endDelivery());
            await AsyncStorage.setItem('@delivery_id', '');
          } else {
            await AsyncStorage.setItem('@delivery_id', deliveryId.toString());
            dispatch(
              activeDeliveryAction.setActiveDelivery({deliveryId: deliveryId}),
            );
          }
          triggerBackgrounfJob(
            deliveryId,
            deliveryId !== previousActiveDeliveryId,
          );
        },
        (error: GeoError) => {
          // See error code charts below.
          console.log(error.code, error.message);
          onToggleSnackBar();
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } else {
      onToggleSnackBar();
    }
  };

  return {
    details,
    status,
    errorMessage,
    activeDeliveryId,
    deliveryId,
    snackBarVisible,
    startBackgroundJob,
    onDismissSnackBar,
  };
};
