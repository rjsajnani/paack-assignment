// Used to get the redux state and define some fucntions that will be used by the deliveres page

// AsynsStorage is used to get the current delivery id value, incase the app is killed we can still
// get the active delivery id

import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNavigation} from '@react-navigation/native';
import {useCallback, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {fetchDeliveries, fetchDetails} from '../../../services';
import {RootState} from '../../../store';
import {activeDeliveryAction} from '../../../store/slice/activeDelivery';

export const useDeliveries = () => {
  const dispatch = useDispatch();

  const navigation = useNavigation();

  const {deliveries, status, activeDeliveryId} = useSelector(
    (state: RootState) => ({
      deliveries: state.deliveries.deliveries,
      status: state.deliveries.status,
      activeDeliveryId: state.activeDelivery.deliveryId,
    }),
  );

  useEffect(() => {
    dispatch(fetchDeliveries());
  }, [dispatch]);

  const getData = useCallback(async () => {
    try {
      const value = await AsyncStorage.getItem('@delivery_id');
      if (value !== null) {
        dispatch(
          activeDeliveryAction.setActiveDelivery({
            deliveryId: parseInt(value, 10),
          }),
        );
      }
    } catch (e) {
      // error reading value
    }
  }, [dispatch]);

  useEffect(() => {
    getData();
  }, [getData]);

  const redirectToDetails = (id: number) => {
    dispatch(fetchDetails(id));
    navigation.navigate('DeliveryDetails');
  };

  return {
    deliveries,
    status,
    activeDeliveryId,
    redirectToDetails,
  };
};
