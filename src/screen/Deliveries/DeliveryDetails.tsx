// Used to display the details of a particular delivery

// Has a button to start/end a delivery

// Once the start button is triggered it calls the startBackgroundJob fucntion to collect tracking info

import React, {FC} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button, Colors, Snackbar} from 'react-native-paper';

import DetailItem from './components/DetailItem';
import {ErrorText, LoadingScreen} from '../../components';
import {useDeliveryDetails} from './hooks/useDeliveryDetails';

const detailsList = [
  'id',
  'Address',
  'Latitude',
  'Longitude',
  'Customer Name',
  'Signature Required',
  'Special Instructions',
];

// create a component
const DeliveryDetails: FC<{}> = () => {
  const {
    details,
    status,
    activeDeliveryId,
    deliveryId,
    errorMessage,
    snackBarVisible,
    onDismissSnackBar,
    startBackgroundJob,
  } = useDeliveryDetails();

  const renderDetails = () => {
    if (status !== 'error') {
      return Object.values(details).map((detail, index) => {
        if (detailsList[index] !== 'id') {
          return (
            <DetailItem key={index} label={detailsList[index]} value={detail} />
          );
        }
      });
    } else {
      return <ErrorText message={errorMessage} />;
    }
  };

  return (
    <View style={styles.container}>
      {status === 'loading' ? (
        <LoadingScreen />
      ) : (
        <>
          {renderDetails()}
          <Button
            mode="contained"
            style={[
              styles.btn,
              activeDeliveryId === deliveryId && styles.activeBtn,
            ]}
            onPress={() => startBackgroundJob()}>
            {activeDeliveryId === deliveryId
              ? 'End Delivery'
              : 'Start Delivery'}
          </Button>
        </>
      )}
      <Snackbar
        visible={snackBarVisible}
        onDismiss={onDismissSnackBar}
        action={{
          label: 'Hide',
          onPress: () => {
            onDismissSnackBar();
          },
        }}>
        Oops unable to get location, please check settings before starting
      </Snackbar>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  btn: {
    marginVertical: 10,
  },
  activeBtn: {
    backgroundColor: Colors.red400,
  },
});

//make this component available to the app
export default DeliveryDetails;
