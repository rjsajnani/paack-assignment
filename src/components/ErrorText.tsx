// General error text to be display

import React, {FC} from 'react';
import {StyleSheet, Text} from 'react-native';
import {Colors} from 'react-native-paper';

export const ErrorText: FC<{message: string | undefined}> = ({message}) => {
  return <Text style={styles.error}> {message} </Text>;
};

const styles = StyleSheet.create({
  error: {
    fontSize: 18,
    textAlign: 'center',
    color: Colors.red400,
  },
});
