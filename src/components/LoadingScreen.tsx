// Common loading/activity indicator used

import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {ActivityIndicator, Colors} from 'react-native-paper';

export const LoadingScreen = () => (
  <View style={styles.loadingContainer}>
    <ActivityIndicator size="large" animating={true} color={Colors.blue400} />
  </View>
);

const styles = StyleSheet.create({
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
});
