import {mockDeliveries} from '../../../mock';
import {
  deliveriesAction,
  deliveriesReducer,
  DeliveriesState,
} from '../deliveriesSlice';

const mockEmptyState: DeliveriesState = {
  status: 'idle',
  deliveries: [],
};

describe('deliveriesSlice', () => {
  describe('deliveriesReducer', () => {
    it('it should handle getDeliveriesRequest action', () => {
      const newState = deliveriesReducer(
        mockEmptyState,
        deliveriesAction.getDeliveriesRequest(),
      );
      expect(newState).toEqual({
        ...mockEmptyState,
        status: 'loading',
      });
    });

    it('it should handle getDeliveriesSuccess action', () => {
      const newState = deliveriesReducer(
        mockEmptyState,
        deliveriesAction.getDeliveriesSuccess({deliveries: [mockDeliveries]}),
      );
      expect(newState).toEqual({
        status: 'success',
        deliveries: [mockDeliveries],
      });
    });

    it('it should handle getDeliveriesFailed action', () => {
      const mockError = 'Unexpected error occurred';
      const newState = deliveriesReducer(
        mockEmptyState,
        deliveriesAction.getDeliveriesFailed({errorMessage: mockError}),
      );
      expect(newState).toEqual({
        ...mockEmptyState,
        status: 'error',
        errorMessage: mockError,
      });
    });
  });
});
