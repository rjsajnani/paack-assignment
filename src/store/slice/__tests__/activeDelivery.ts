import {mockActiveDelivery} from '../../../mock';
import {
  activeDeliveryAction,
  activeDeliveryReducer,
  ActiveDeliveryState,
} from '../activeDelivery';

const mockEmptyState: ActiveDeliveryState = {
  deliveryId: '',
};

describe('activeDeliverySlice', () => {
  describe('activeDeliveryReducer', () => {
    it('it should handle setActiveDelivery action', () => {
      const newState = activeDeliveryReducer(
        mockEmptyState,
        activeDeliveryAction.setActiveDelivery(mockActiveDelivery),
      );
      expect(newState).toEqual({
        ...mockEmptyState,
        deliveryId: mockActiveDelivery.deliveryId,
      });
    });

    it('it should handle endDelivery action', () => {
      const newState = activeDeliveryReducer(
        mockEmptyState,
        activeDeliveryAction.endDelivery(),
      );
      expect(newState).toEqual({
        ...mockEmptyState,
      });
    });
  });
});
