import {mockDeliveryDetails} from '../../../mock';
import {
  deliveryDetailsAction,
  deliveryDetailsReducer,
  DeliveryDetailsState,
} from '../deliveryDetailsSlice';

const mockEmptyState: DeliveryDetailsState = {
  status: 'idle',
  details: {},
};

describe('deliveryDetailsSlice', () => {
  describe('deliveryReducer', () => {
    it('it should handle getDetailsRequest action', () => {
      const newState = deliveryDetailsReducer(
        mockEmptyState,
        deliveryDetailsAction.getDetailsRequest(),
      );
      expect(newState).toEqual({
        ...mockEmptyState,
        status: 'loading',
      });
    });

    it('it should handle getDetailsSuccess action', () => {
      const newState = deliveryDetailsReducer(
        mockEmptyState,
        deliveryDetailsAction.getDetailsSuccess({details: mockDeliveryDetails}),
      );
      expect(newState).toEqual({
        status: 'success',
        details: mockDeliveryDetails,
      });
    });

    it('it should handle getDetailsFailed action', () => {
      const mockError = 'Unexpected error occurred';
      const newState = deliveryDetailsReducer(
        mockEmptyState,
        deliveryDetailsAction.getDetailsFailed({errorMessage: mockError}),
      );
      expect(newState).toEqual({
        ...mockEmptyState,
        status: 'error',
        errorMessage: mockError,
      });
    });
  });
});
