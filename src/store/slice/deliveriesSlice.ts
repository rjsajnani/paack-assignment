// Here we have created the deliveries action and reducer

import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {DeliveriesObject} from '../../types/deliveries';

export type DeliveriesState = {
  status: 'idle' | 'loading' | 'success' | 'error';
  deliveries: DeliveriesObject[];
  errorMessage?: string;
};

const initialState: DeliveriesState = {
  status: 'idle',
  deliveries: [],
};

const deliveriesSlice = createSlice({
  name: 'deliveries',
  initialState,
  reducers: {
    getDeliveriesRequest(state) {
      state.status = 'loading';
    },
    getDeliveriesSuccess(
      state,
      action: PayloadAction<{deliveries: DeliveriesObject[]}>,
    ) {
      const {deliveries} = action.payload;
      state.status = 'success';
      state.deliveries = deliveries;
    },
    getDeliveriesFailed(state, action: PayloadAction<{errorMessage: string}>) {
      const {errorMessage} = action.payload;
      state.errorMessage = errorMessage;
      state.status = 'error';
    },
  },
});

export const deliveriesReducer = deliveriesSlice.reducer;
export const deliveriesAction = deliveriesSlice.actions;
