// Here we have created the delivery details action and reducer

import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {mockDeliveryDetails} from '../../mock';
import {DeliveryDetails} from '../../types/deliveries';

export type DeliveryDetailsState = {
  status: 'idle' | 'loading' | 'success' | 'error';
  details: {};
  errorMessage?: string;
};

const initialState: DeliveryDetailsState = {
  status: 'idle',
  details: mockDeliveryDetails,
};

const deliveryDetailsSlice = createSlice({
  name: 'details',
  initialState,
  reducers: {
    getDetailsRequest(state) {
      state.status = 'loading';
    },
    getDetailsSuccess(
      state,
      action: PayloadAction<{details: DeliveryDetails}>,
    ) {
      const {details} = action.payload;
      state.details = details;
      state.status = 'success';
    },
    getDetailsFailed(state, action: PayloadAction<{errorMessage: string}>) {
      const {errorMessage} = action.payload;
      state.errorMessage = errorMessage;
      state.status = 'error';
    },
  },
});

export const deliveryDetailsReducer = deliveryDetailsSlice.reducer;
export const deliveryDetailsAction = deliveryDetailsSlice.actions;
