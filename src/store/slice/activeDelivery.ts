// Here we have created the active delivery's action and reducer

import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export type ActiveDeliveryState = {
  deliveryId: number | '';
};

const initialState: ActiveDeliveryState = {
  deliveryId: '',
};

const activeDelivery = createSlice({
  name: 'activeDelivery',
  initialState,
  reducers: {
    setActiveDelivery(state, action: PayloadAction<{deliveryId: number}>) {
      const {deliveryId} = action.payload;
      state.deliveryId = deliveryId;
    },
    endDelivery(state) {
      state.deliveryId = '';
    },
  },
});

export const activeDeliveryReducer = activeDelivery.reducer;
export const activeDeliveryAction = activeDelivery.actions;
