// Redux store config

import {
  configureStore,
  getDefaultMiddleware,
  Middleware,
} from '@reduxjs/toolkit';
import {
  activeDeliveryReducer,
  ActiveDeliveryState,
} from './slice/activeDelivery';
import {deliveriesReducer, DeliveriesState} from './slice/deliveriesSlice';
import {
  deliveryDetailsReducer,
  DeliveryDetailsState,
} from './slice/deliveryDetailsSlice';

export type RootState = {
  deliveries: DeliveriesState;
  deliveryDetails: DeliveryDetailsState;
  activeDelivery: ActiveDeliveryState;
};

export const store = configureStore<RootState, Middleware>({
  reducer: {
    deliveries: deliveriesReducer,
    deliveryDetails: deliveryDetailsReducer,
    activeDelivery: activeDeliveryReducer,
  },
  middleware: [...getDefaultMiddleware({immutableCheck: false})],
});
