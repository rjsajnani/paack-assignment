// Mainly used to mock some test cases

import {ActiveDeliveryState} from '../store/slice/activeDelivery';
import {DeliveriesObject, DeliveryDetails} from '../types/deliveries';

export const mockDeliveries: DeliveriesObject = {
  id: 1,
  address:
    'Kiara Residence 2, Bukit Jalil, 58200 Kuala Lumpur, Federal Territory of Kuala Lumpur',
  latitude: 3.064680500799499,
  longitude: 101.66943388524061,
  customer_name: 'Raj',
};

export const mockDeliveryDetails: DeliveryDetails = {
  id: 1,
  address:
    'Kiara Residence 2, Bukit Jalil, 58200 Kuala Lumpur, Federal Territory of Kuala Lumpur',
  latitude: 3.064680500799499,
  longitude: 101.66943388524061,
  customer_name: 'Raj',
  requires_signature: false,
  special_instructions:
    'blue door, besides the car shop. Deliver to the car shop if necessary',
};

export const mockActiveDelivery: ActiveDeliveryState = {
  deliveryId: 1,
};
