/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {Linking, StatusBar} from 'react-native';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {Provider} from 'react-redux';

import {store} from './src/store';

import {NavigationContainer} from '@react-navigation/native';
import {MainStackNavigator} from './src/navigation';
import {useApp} from './useApp';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  dark: false,
  colors: {
    ...DefaultTheme.colors,
    primary: '#00a3e0',
  },
};

const App = () => {
  const {requestPermission} = useApp();

  React.useEffect(() => {
    requestPermission();
    Linking.addEventListener('url', (e) => {
      handleOpenURL({url: e.url});
    });
  }, [requestPermission]);

  const handleOpenURL = ({url}: any) => {
    // Will be called when the notification is pressed
    console.log(url, 'handling deeplink');
    // do something
  };
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <StatusBar barStyle="dark-content" />
        <NavigationContainer>
          <MainStackNavigator />
        </NavigationContainer>
      </PaperProvider>
    </Provider>
  );
};

export default App;
